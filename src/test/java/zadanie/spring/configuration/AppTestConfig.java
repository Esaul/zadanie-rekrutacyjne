package zadanie.spring.configuration;

import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppTestConfig extends AppConfig {

    public static void main(String[] args) {
        SpringApplication.run(AppTestConfig.class, args);
    }
}
