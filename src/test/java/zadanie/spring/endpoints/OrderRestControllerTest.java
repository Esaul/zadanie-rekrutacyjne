package zadanie.spring.endpoints;

import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import zadanie.spring.configuration.AppTestConfig;

@ContextConfiguration(classes = AppTestConfig.class)
@WebMvcTest(OrderRestController.class)
@WithMockUser
public class OrderRestControllerTest {
}
