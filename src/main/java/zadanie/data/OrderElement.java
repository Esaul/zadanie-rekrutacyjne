package zadanie.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/*

Zamówienie posiada pola przechowujące imię, nazwisko, adres, kod pocztowy,
numer telefonu, listę pozycji ( ilość, id z zewnętrznej aplikacji obsługującej menu)


lista pozycji

 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderElement {
    private Order order;
    private Long itemId;
    private Double amount;
}
