package zadanie.data;

/*

Zamówienia mogą być w statusie "Nowe", "Złożone", "Odrzucone", "Usunięte", "Zrealizowane"

*/

public enum OrderStatus {
    NEW("Nowe"),
    PLACED("Złożone"),
    REJECTED("Odrzucone"),
    DELETED("Usunięte"),
    REALISED("Zrealizowane");

    String statusName;

    OrderStatus(String statusName) {
        this.statusName = statusName;
    }
}
