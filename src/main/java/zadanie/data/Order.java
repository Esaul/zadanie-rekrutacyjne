package zadanie.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/*

Zamówienie posiada pola przechowujące imię, nazwisko, adres, kod pocztowy,
numer telefonu, listę pozycji ( ilość, id z zewnętrznej aplikacji obsługującej menu)

 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Order {
    private String firstName;
    private String lastName;
    private String address;
    private String postCode;
    private String phoneNo;
    private List<OrderElement> orderElements;

}
