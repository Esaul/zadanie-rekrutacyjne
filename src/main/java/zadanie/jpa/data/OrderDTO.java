package zadanie.jpa.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import zadanie.data.OrderStatus;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class OrderDTO {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "order_id")
    private Long id;

    private String firstName;
    private String lastName;
    private String address;
    private String postCode;
    private String phoneNo;
    private OrderStatus orderStatus;

    @OneToMany(targetEntity = OrderElementDTO.class)
    @JoinColumn(name= "orderId")
    private List<OrderElementDTO> orderElements;
}
