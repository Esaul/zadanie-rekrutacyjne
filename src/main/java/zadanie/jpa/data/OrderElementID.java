package zadanie.jpa.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
/*
Wybrałem użycie kompozytowego klucza ponieważ wielokrotne dodanie tego samego produktu można
scalić. Inne rozwiązania jakie przychodzą mi do głowy:
- Dodanie osobnego klucza dla tabeli elementów zamówienia
- nieużywanie klucza
, z czego nieużywanie klucza jest rozwiązaniem powodującym dużo problemów, a rozwiązanie z osobnym
kluczem wg. mnie niepotrzebnie poszerza tabelę.
 */
public class OrderElementID implements Serializable {
    private Long orderId;
    private Long itemId;
}
