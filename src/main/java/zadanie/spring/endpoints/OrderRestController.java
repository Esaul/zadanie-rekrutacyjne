package zadanie.spring.endpoints;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/*
Serwis eksponuje REST API z metodami pozwalającymi na:
-stworzenie zamówienia
-listę zamówień z filtrem po statusie
-pobranie jednego zamówienia po id.
-usunięcie zamówienia w statusie "Nowy"
-zmianę statusu na "Zrealizowane"

Zamówienie jest tworzone w statusie "Nowe"
Dla Zamienia sprawdzany jest, czy adres podany w zamówienie znajduje się w Obszarze dostawy.
(Obszar dostawy podany jest w konfiguracji jako lista punktów tworzących wielokąt)

Jeśli adres jest w obszarze, Zamówienie jest zapisane w bazie jako "Złożone", w przeciwnym wypadku jako "Odrzucone".

 */

@RestController
@RequestMapping("order")
public class OrderRestController {
}
