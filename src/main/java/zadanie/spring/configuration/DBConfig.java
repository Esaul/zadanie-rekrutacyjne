package zadanie.spring.configuration;

import org.h2.Driver;
import org.h2.server.web.WebServlet;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.dialect.H2Dialect;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableJpaRepositories("zadanie.spring.repositories")
public class DBConfig {

	@Bean
	@Primary
	public DataSource dataSource() {
		return DataSourceBuilder
				.create()
				.username("username")
				.password("password")
				.url("jdbc:h2:mem:baza")
				.driverClassName(Driver.class.getName())
				.build();
	}

	@Bean
	public ServletRegistrationBean<WebServlet> h2servletRegistration() {
		ServletRegistrationBean<WebServlet> registration = new ServletRegistrationBean<>(new WebServlet());
		registration.addUrlMappings("/console/*");
		return registration;
	}

	@Bean
	LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource) {
		LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
		entityManagerFactoryBean.setDataSource(dataSource);
		entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
		entityManagerFactoryBean.setPackagesToScan("zadanie.jpa.data");

		Properties jpaProperties = new Properties();

		jpaProperties.put(AvailableSettings.DIALECT, H2Dialect.class.getName());
		jpaProperties.put(AvailableSettings.HBM2DDL_AUTO, "update");
		jpaProperties.put(AvailableSettings.SHOW_SQL, true);
		jpaProperties.put(AvailableSettings.FORMAT_SQL, true);
		jpaProperties.put(AvailableSettings.GLOBALLY_QUOTED_IDENTIFIERS, true);

		entityManagerFactoryBean.setJpaProperties(jpaProperties);

		return entityManagerFactoryBean;
	}
}
