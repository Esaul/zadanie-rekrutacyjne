package zadanie.spring.configuration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@Configuration
@SpringBootApplication(
    scanBasePackages = {
        "zadanie.spring.configuration",
        "zadanie.spring.endpoints",
        "zadanie.spring.repositories"
    }
)
public class AppConfig {
    public static void main(String[] args) {
        SpringApplication.run(AppConfig.class, args);
    }
}
