package zadanie.spring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Controller;
import zadanie.jpa.data.OrderElementDTO;
import zadanie.jpa.data.OrderElementID;

@Controller
public interface OrderElementRepository extends JpaRepository<OrderElementDTO, OrderElementID> {
}
