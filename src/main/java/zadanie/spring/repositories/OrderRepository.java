package zadanie.spring.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Controller;
import zadanie.jpa.data.OrderDTO;

@Controller
public interface OrderRepository extends JpaRepository<OrderDTO, Long> {
}
